
const functiones = require('../api/api')

let lat = 0;
let long = 0;


function setAlerta() {
    if (lat == 0 && long == 0)
        window.alert("Error. Elige un punto del mapa");
    else {
        let name = document.getElementById("Deskripzioa").value;
        let city = document.getElementById("city").value;
        let status = 1;
        functiones.setAlert(name, city, lat, long, status).then((response) => {
            sendNotification({
                name: name,
                city: city,
                lat: lat,
                long: long,
                status: status
            })
            window.location.href = "lista.html";
        })

    }

}

var mymap = L.map('mapid').setView([43.041, -2.65], 8);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    id: 'mapbox/streets-v11'
}).addTo(mymap);

var marker;
function onMapClick(e) {
    long = e.latlng.lng
    lat = e.latlng.lat
    if (marker != undefined) {
        mymap.removeLayer(marker);
    };
    marker = L.marker([lat, long]).addTo(mymap);
}

mymap.on('click', onMapClick);


//**NOTIFICATION**/

var io = require('socket.io-client');
var socket = io.connect("http://127.0.0.1:2222");
socket.on("connect", function () {
    console.log("Electron conectado para enviar notificaciones 2222")
});
function sendNotification(data) {
    socket.emit('notification', data);
}