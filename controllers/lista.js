//const functiones = require('../api')
const functiones = require('../api/api')
const storage = require('electron-json-storage');

var selectedAlert = '';
crearTabla();

function crearTabla() {
    functiones.getAlerts().then((response) => {
        let tbody = ''
        let theader = '';
        document.getElementById('body').innerHTML = '';
        for (let i = 0; i < response.json.length; i++) {
            tbody += '<tr>'
            tbody += '<td>' + response.json[i].name + '</td>'
            tbody += '<td>' + response.json[i].city + '</td> '
            tbody += '<td>' + response.json[i].status + '</td>'
            
            tbody += '<td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" '
            tbody += " onClick='editAlert(\"" + response.json[i].name + "\")'";
            tbody += ' data-toggle="modal" data-target="#edit"><span class="glyphicon glyphicon-pencil"></span></button></p></td>'

            tbody += '<td> <p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" '
            tbody += " onClick='fdeleteAlert(\"" + response.json[i]._id + "\")'";
            tbody += ' data-title="Delete" data-toggle="modal" data-target="#delete"><span class="glyphicon glyphicon-trash"></span></button></p></td>'

            tbody += '<td> <p data-placement="top" data-toggle="tooltip" title="More Info"><button class="btn btn-info btn-xs" '
            tbody += " onClick='moreInfo(\"" + response.json[i]._id + "\")'";
            tbody += ' data-title="More Info" data-toggle="modal" data-target="#delete"><span class="glyphicon glyphicon-info-sign"></span></button></p></td>'
            tbody += '</tr>';
        }
        let tfooter = '</table></div>';
        document.getElementById('body').innerHTML = theader + tbody + tfooter;
    })
}



function fdeleteAlert(id) {
    functiones.deleteAlert(id).then((response) => {
        this.crearTabla();
    })

}

function editAlert(id) {
    selectedAlert = id;
    var modal = document.getElementById("myModal");
    modal.style.display = "block";
}

function changeStatus() {

    let status = 1;
    if (document.getElementById('inlineRadio1').checked) status = 1;
    if (document.getElementById('inlineRadio2').checked) status = 0;
    modal.style.display = "none";
    functiones.changeAlertStatus(selectedAlert, status).then((response) => {
        this.crearTabla();
    })
}


function redirect() {
    window.location.href = "form.html";
}

function moreInfo(id) {

    storage.set('nameAlert', { nameAlert: id }, function (error) {
        if (error) {
            throw error;
        }
    });

    window.location.href = "detalleAlerta.html"
}



