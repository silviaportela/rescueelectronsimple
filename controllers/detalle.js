const functiones = require('../api/api')
const storage = require('electron-json-storage');
var popup;
var zone;
var mymap;
var nuevaZona=[

]

function anadirZona(){
    var valor=document.getElementById("span01").innerHTML;
    
    if (valor == "Añadir Zona")
    {
        zone=true
        mymap.on('click', onMapClick);
    }
    else{
        subirZona();
    }

    
}

function finZona(){
    zone=false
    nuevaZona=[]
}

function subirZona(){
    var coordenadas= nuevaZona
    storage.get('nameAlert', function (error, data) {
        setZone(data.nameAlert,coordenadas).then((response)=>
        {
            finZona();
        }) 
    })

   
}

function onMapClick(e) {
    if (zone){
        var coordenada=[e.latlng.lat,e.latlng.lng]
        nuevaZona.push(coordenada);
        var polygon = L.polygon(nuevaZona, { color: 'red' }).addTo(mymap);
        console.log("onMapClick nueva zona",nuevaZona)
    }
   
}

function formatTime(time) {
    time = time / 1000
    var days = Math.floor(time / 86400);
    var hours = Math.floor((time % 86400) / 3600);
    var minutes = Math.floor((time % 3600) / 60);
    var seconds = time % 60;
    minutes = minutes < 10 ? 0 + minutes : minutes;
    seconds = seconds < 10 ? 0 + seconds : seconds;
    var result = days + " días "+ hours + " horas " + minutes + " minutos";  
    return result.toString();
}



//PINTAR EL MAPA CON LA INFO
storage.get('nameAlert', function (error, data) {
    if (error) throw error;

    functiones.getOneAlert(data.nameAlert).then((response) => {
  
        //MAPA
        mymap = L.map('mapid').setView([response.json[0].coord1, response.json[0].coord2], 8);
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            id: 'mapbox/streets-v11'
        }).addTo(mymap);

        popup = L.popup()
        .setLatLng([51.5, -0.09])
        .setContent("I am a standalone popup.")
        .openOn(mymap);

        document.getElementById('personas').innerHTML = '';      
        let p = '<p>'+response.json[0].count+' personas participan en la búsqueda<p>'
        document.getElementById('personas').innerHTML = p;
        document.getElementById('horas').innerHTML = '';      
        p = '<p> Tiempo acumulado: '+formatTime(response.json[0].dateDifference)+' <p>'
        document.getElementById('horas').innerHTML = p;

        //MARKER
        console.log(response.json[0].coord1)
        console.log(response.json[0].coord2)
        var marker =  L.marker([response.json[0].coord1, response.json[0].coord2]).addTo(mymap);
        marker.bindPopup("<b>¡Alerta!</b><br>" + response.json[0].name).openPopup();
        var circle = L.circle([response.json[0].coord1, response.json[0].coord2], {
            color: 'yellow',
            fillColor: 'yellow',
            fillOpacity: 0.5,
            radius: 5000
        }).addTo(mymap);
        mymap.fitBounds(circle.getBounds());

        //Puntos interes
        console.log("response.json[0]",response.json[0].puntosImportantes)
        if(response.json[0].puntosImportantes.length>0){
            for (let i = 0; i < response.json[0].puntosImportantes.length; i++) {
                var marker =  L.marker([response.json[0].puntosImportantes[i][0], response.json[0].puntosImportantes[i][1]]).addTo(mymap);
                marker.bindPopup("<b>Punto de Socorro</b><br>").openPopup();
            }
        }
      

        //TRACKS
        functiones.getTracksFromAlert(data.nameAlert).then((response) => {
            for (let i = 0; i < response.json.length; i++) {
                var coordenadas=[]
                for (let j = 0; j < response.json[i].coordinates.length; j++) {
                    var coordenadasAux = [response.json[i].coordinates[j].latitude,response.json[i].coordinates[j].longitude]
                    coordenadas.push(coordenadasAux)
                }
                var polyline = L.polyline(coordenadas, { color: 'red' }).addTo(mymap);

            }
        })

        //ZONES
        functiones.getZonesFromAlert(data.nameAlert).then((response) => {
            for (let i = 0; i < response.json.length; i++) {           
                var polygon = L.polygon(response.json[i].coordinates, { color: 'yellow' }).addTo(mymap);
            }
        })



    })
});