
const functiones = require('../api/api')

var mymap = L.map('mapid').setView([43.041, -2.65], 8);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    id: 'mapbox/streets-v11'
}).addTo(mymap);
functiones.getAlerts().then((response) => {
    var marker
    for (let i = 0; i < response.json.length; i++) {
        marker = L.marker([response.json[i].coord1, response.json[i].coord2]).addTo(mymap);
        marker.bindPopup("<b>¡Alerta!</b><br>" + response.json[i].name).openPopup();
    }
})

function redirect() {
    var url = "form.html";
    window.location.href = url;

}


// var popup = L.popup()
//     .setLatLng([51.5, -0.09])
//     .setContent("I am a standalone popup.")
//     .openOn(mymap);

// function onMapClick(e) {
//     popup
//         .setLatLng(e.latlng)
//         .setContent("You clicked the map at " + e.latlng.toString())
//         .openOn(mymap);
// }

// mymap.on('click', onMapClick);