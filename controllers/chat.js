
var me = {};
me.avatar = "https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48";

var you = {};
you.avatar = "https://a11.t26.net/taringa/avatares/9/1/2/F/7/8/Demon_King1/48x48_5C5.jpg";

function formatAMPM(date) {
    let strTime = date.getHours() + ":" + date.getMinutes()
    strTime = strTime + " (" + date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + ")";
    return strTime;
}

function insertChat(who, text, time) {
    if (time === undefined) {
        time = 0;
    }
    var control = "";
    if (time == 0) var date = formatAMPM(new Date());
    else {
        var time2 = parseInt(time)
        var date = formatAMPM(new Date(time2));
    }

    if (who == "me") {
        control = '<li style="width:100%">' +
            '<div class="msj macro">' +
            '<div class="avatar"><img class="img-circle" style="width:100%;" src="' + me.avatar + '" /></div>' +
            '<div class="text text-l">' +
            '<p>' + text + '</p>' +
            '<p><small>' + date + '</small></p>' +
            '</div>' +
            '</div>' +
            '</li>';
    } else {
        control = '<li style="width:100%;">' +
            '<div class="msj-rta macro">' +
            '<div class="text text-r">' +
            '<p>' + text + '</p>' +
            '<p><small>' + date + '</small></p>' +
            '</div>' +
            '<div class="avatar" style="padding:0px 0px 0px 10px !important"><span> ' + who + '</span>' +
            '</li>';
    }
    setTimeout(
        function () {
            $("#ul").append(control).scrollTop($("#ul").prop('scrollHeight'));
        }, 0);

}

function resetChat() {
    $("#ul").empty();
}

$(".mytext").on("keydown", function (e) {
    if (e.which == 13) {
        var text = $(this).val();
        if (text !== "") {
            insertChat("me", text);
            $(this).val('');
        }
    }
});

$('.chat > div > div > div:nth-child(2) > span').click(function () {
    $(".mytext").trigger({ type: 'keydown', which: 13, keyCode: 13 });
})


const storage = require('electron-json-storage');
const functiones = require('../api/api')


storage.get('nameAlert', function (error, data) {
    if (error) throw error;

    functiones.getMessagesFromAlertID(data.nameAlert).then((response) => {
        for (let i = 0; i < response.json.length; i++) {
            console.log("response.json", response.json[i])
            if (response.json[i].user != "admin") {
                insertChat(response.json[i].user, response.json[i].message, response.json[i].timestamp)
            } else {
                insertChat("me", response.json[i].message, response.json[i].timestamp)
            }
        }

    })
});

function mandarMensaje() {
    storage.get('nameAlert', function (error, data) {
        if (error) throw error;

        var idAlert = data.nameAlert
        var time = new Date().getTime();
        var message = document.getElementById("span01").value;
        if (message != "")
        functiones.sendMessage(idAlert, message, time).then((response) => {
                insertChat("me", message, time)
                var data = {
                    user: "admin",
                    idAlert: idAlert,
                    timestamp: time,
                    message: message
                }
                sendMensaje(data)
                document.getElementById("span01").value = "";
            })
        document.getElementById("span01").innerHTML = "";
    });


}


//Socket io--CHAT
var io = require('socket.io-client');
var socket = io.connect("http://127.0.0.1:2222");

socket.on("connect", function () {
    console.log("Electron conectado al servidor en el puerto 2222")
});

socket.on("mensaje", function (data) {
    console.log("Servidor: " + data)
});


socket.on("broadcast", function (data) {
    console.log("**********");
    console.log("He recibido mensaje de " + data.user);
    console.log("Para la alerta " + data.idAlert);
    console.log("A dia de " + data.timestamp);
    console.log("Que dice: " + data.message);
    console.log("**********");
    storage.get('nameAlert', function (error, data2) {
        var idAlert = data2.nameAlert
        if (data.user != "admin" && data.idAlert==idAlert){
            insertChat(data.user, data.message, data.timestamp)
    }
})
});

socket.on("disconnect", function () {
    console.log("Desconectado: ");
});



function sendMensaje(data) {
    socket.emit('mensaje From client', data);
}

