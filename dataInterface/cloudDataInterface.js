const api = require('../api')

export function getUsers(name) {
    return api.getUsers(name);
}

export function getAlerts() {
    return api.getAlerts();
}

export function getOneAlert(name) {
    return api.getOneAlert(name);
}

export function setAlert(name,city,lat,long) {
    return api.setAlert(name,city,lat,long);
}

export function changeAlertStatus(name,status) {
    return api.changeAlertStatus(name,status);
}

export function getTracksFromAlert(alertid) {
    return api.getTracksFromAlert(alertid)
}

export function deleteAlert(id) {
    return api.deleteAlert(id);
}

export function setZone(alertid, coordenadas) {
    return api.setZone(alertid, coordenadas);
}

export function getZonesFromAlert(alertid) {
    return api.getZonesFromAlert(alertid);
}

export function getMessagesFromAlertID(alertid) {
    return api.getMessagesFromAlertID(alertid);
}

export function sendMessage(alertid, message, timestamp) {
    return api.sendMessage(alertid, message, timestamp);
}





