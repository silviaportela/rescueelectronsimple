
const hosts=  "http://127.0.0.1:3000/"
const endPoints = {
    getAlerts:'alerts/',
    putAlerts:'alerts/putAlert/',
    getUsers:'user/',
    getTracks:'tracks/',
    getMessages:'messages/',
     getZones:'zonas/',
}
function getUsers(name) {
    let url = hosts+"user/" + name;
    return getData(url)
}

function getAlerts() {
    let url = hosts+endPoints.getAlerts;
    return getData(url)
}


function getOneAlert(name) {
    let url = hosts+endPoints.getAlerts+name;
    return getData(url)
}

function setAlert(name,city,lat,long) {
    let url = hosts+endPoints.getAlerts;
    return postData(url,{
        "name": name,
        "status": 1,
        "coord1": lat,
        "coord2": long,
        "city": city,
    })
}

function deleteAlert(id) {
    let url = hosts+endPoints.getAlerts+id;
    return deleteData(url)
}

function changeAlertStatus(name,status) {
    let url = hosts+endPoints.putAlerts+name;
    return putData(url,{
        "status":status
    })
}

function getTracksFromAlert(alertid) {
    let url = hosts+endPoints.getTracks+alertid;
    return getData(url)
}

function setZone(alertid,coordenadas) {
    let url = hosts+endPoints.getZones;
    return postData(url,{
        "idAlert":alertid,
        "coordinates":coordenadas})
}

function getZonesFromAlert (alertid){
    let url = hosts+endPoints.getZones+alertid;
    return getData(url)
}

function getMessagesFromAlertID (alertid){
    let url =hosts+endPoints.getMessages+alertid;
    return getData(url)
}

function sendMessage (alertid,message,timestamp){
    let url = hosts+endPoints.getMessages;
    return postData(url,{
        idAlert: alertid,
        user:"admin",
        timestamp: timestamp,
        message:message
    })
}



//***********************///

function getData(url) {
    console.log("getdataURL"+url)
    return fetch(url, {
        method: 'GET',
    }).then((response) => {
        console.log("response",response)
        if (response.status === 200) {
            return response.json();
        } else {
            return response.status;
        }
    })
}

 function postData(url, body) {      
     console.log("BODY DE fetch_Auth_POST url:", url)
     return fetch(url, {
         method: 'POST',
         body: JSON.stringify(body),
         headers:{
             'Content-Type': 'application/json'
           }
     }).then((response) => {
         if (response.status === 201) {
             return response.json();
         } else {
             return response.status;
         }
     });
 }
    
 function deleteData(url) {
    console.log("BdeleteData url:", url)
    return fetch(url, {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json'},
        body: JSON.stringify(body),
    }).then((response) => {
        console.log("deleteresponse",response)
        if (response.status === 200) {
            return response.json();
        } else {
            return response.status;
        }
    });
}

function putData(url, body) {
    console.log("BODY fetch_Auth_PUT body:", JSON.stringify(body))
    console.log("url fetch_Auth_PUT url:", url)
    return fetch(url, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json'},
        body: JSON.stringify(body),
    }).then((response) => {
        // console.log("response:",response)
        if (response.status === 200) {
            return response.json();
        } else {
            return response.status;
        }
    });
}

module.exports={
    getUsers:getUsers,
    getAlerts:getAlerts,
    setAlert:setAlert,
    deleteAlert:deleteAlert,
    changeAlertStatus:changeAlertStatus,
    getOneAlert:getOneAlert,
    getTracksFromAlert:getTracksFromAlert,
    setZone:setZone,
    getZonesFromAlert:getZonesFromAlert,
    getMessagesFromAlertID:getMessagesFromAlertID,
    sendMessage:sendMessage
    
}


    