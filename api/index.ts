

// import { hosts, endPoints } from './config';
//import { postData, getData, putData, deleteData } from './utils'
const data = require('./config')
const hosts = data.hosts
const endPoints = data.endPoints
const utils = require('./utils')

class Platform {

    private host;
    constructor() {
        this.host = hosts.dev;
    }


    async getUsers(name: String) {
        try {
            const response = await utils.getData(`${this.host}${endPoints.getUsers}` + name);
            console.log("GET ZONES", response)
            return response;
        }
        catch (error) {
            return 333;
        }
    }

    async getAlerts() {
        try {
            const response = await utils.getData(`${this.host}${endPoints.getAlerts}`);
            console.log("getAlerts", response)
            return response;
        }
        catch (error) {
            return 333;
        }
    }

    async getOneAlert(name: String) {
        try {
            const response = await utils.getData(`${this.host}${endPoints.getAlerts}` + name);
            console.log("GET ZONES", response)
            return response;
        }
        catch (error) {
            return 333;
        }
    }

    async setAlert(name, city, lat, long) {
        try {
            const response = await utils.postData(`${this.host}${endPoints.getAlerts}`, {
                "name": name,
                "status": 1,
                "coord1": lat,
                "coord2": long,
                "city": city,
            });
            console.log("setAlert", response)
            return response;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    }


    async changeAlertStatus(name: String, status: String) {
        try {
            const response = await utils.putData(`${this.host}${endPoints.putAlert}` + name, {
                "status": status
            });
            return response;
        }
        catch (error) {
            return 333;
        }
    }


    async getTracksFromAlert(alertid: String) {
        try {
            const response = await utils.getData(`${this.host}${endPoints.getTracks}` + alertid);
            return response;
        }
        catch (error) {
            return 333;
        }
    }





    async deleteAlert(id) {
        try {
            const response = await utils.deleteData(`${this.host}${endPoints.getAlerts}` + id)
            return response;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    }






    async setZone(alertid, coordenadas) {
        try {
            const response = await utils.postData(`${this.host}${endPoints.getZones}`, {
                "idAlert": alertid,
                "coordinates": coordenadas
            });
            console.log("setAlert", response)
            return response;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    }

    async getZonesFromAlert(alertid: String) {
        try {
            const response = await utils.getData(`${this.host}${endPoints.getZones}` + alertid);
            return response;
        }
        catch (error) {
            return 333;
        }
    }


    async getMessagesFromAlertID(alertid: String) {
        try {
            const response = await getData(`${this.host}${endPoints.getMessages}` + alertid);
            return response;
        }
        catch (error) {
            return 333;
        }
    }


    async sendMessage(alertid, message, timestamp) {
        try {
            const response = await utils.postData(`${this.host}${endPoints.getMessages}`, {
                idAlert: alertid,
                user: "admin",
                timestamp: timestamp,
                message: message
            });
            console.log("sendMessage", response)
            return response;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    }




}
let platform = new Platform()
export default platform;