export function getData(url) {
    console.log("getdataURL"+url)
    return fetch(url, {
        method: 'GET',
    }).then((response) => {
        console.log("response",response)
        if (response.status === 200) {
            return response.json();
        } else {
            return response.status;
        }
    })
}

export function postData(url, body) {      
     console.log("BODY DE fetch_Auth_POST url:", url)
     return fetch(url, {
         method: 'POST',
         body: JSON.stringify(body),
         headers:{
             'Content-Type': 'application/json'
           }
     }).then((response) => {
         if (response.status === 201) {
             return response.json();
         } else {
             return response.status;
         }
     });
 }
    
export function deleteData(url) {
    console.log("BdeleteData url:", url)
    return fetch(url, {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json'},
        body: JSON.stringify(body),
    }).then((response) => {
        console.log("deleteresponse",response)
        if (response.status === 200) {
            return response.json();
        } else {
            return response.status;
        }
    });
}

export function putData(url, body) {
    console.log("BODY fetch_Auth_PUT body:", JSON.stringify(body))
    console.log("url fetch_Auth_PUT url:", url)
    return fetch(url, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json'},
        body: JSON.stringify(body),
    }).then((response) => {
        // console.log("response:",response)
        if (response.status === 200) {
            return response.json();
        } else {
            return response.status;
        }
    });
}


module.exports={
    deleteData:deleteData,
    getData:getData,
    postData:postData,
    putData:putData
}
